======================================
BubbleDet documentation
======================================

BubbleDet is a Python package for computing functional determinants which
arise in the bubble nucleation rate. The code is open source and free to use.
If you use BubbleDet, please cite `arXiv:2308.15652`_.

.. _arXiv:2308.15652: https://arxiv.org/abs/2308.15652

.. image:: images/bubbles.jpg
    :width: 300
    :align: center
    :alt: Bubbles

|


.. toctree::
    :caption: Getting Started:
    :maxdepth: 2
    :hidden:

    intro
    installation
    first_example
    collected_examples
    faq

.. toctree::
    :caption: API Reference
    :maxdepth: 4
    :hidden:

    bubbledet

.. toctree::
    :caption: Development:
    :maxdepth: 2
    :hidden:

    changelog
    development
    contact
