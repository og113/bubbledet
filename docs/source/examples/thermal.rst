======================================
Thermal nucleation rate
======================================

.. literalinclude:: ../../../examples/thermal.py
   :language: py
