======================================
Development version
======================================

BubbleDet is under active development, with plans for future versions with
additional functionality. The latest developments are available from our
`git repository`_.

.. _git repository: https://bitbucket.org/og113/bubbledet

To install the development version, run the following::

    git clone https://bitbucket.org/og113/bubbledet.git
    cd bubbledet
    pip install -e .[tests]


Tests can then be run with::

    pytest -v

from the root directory of the repository.
