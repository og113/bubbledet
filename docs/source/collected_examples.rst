======================================
Collected examples
======================================

.. toctree::
    :maxdepth: 2

    examples/analogue
    examples/first_example
    examples/kink
    examples/symmetry_breaking
    examples/thermal
    examples/thinwall
    examples/unbounded
