===========================================
Installation
===========================================

BubbleDet is based on Python 3. The necessary libraries are installed
automatically when using pip or conda to install BubbleDet. For a list
see `pyproject.toml` in the `git repository`_.

.. _git repository: https://bitbucket.org/og113/bubbledet


Installing with pip
===========================================

Can be installed as a package with pip, or pip3, using::

    pip install BubbleDet

See the `BubbleDet PyPI page`_ for further information.

.. _BubbleDet PyPI page: https://pypi.org/project/BubbleDet


Installing with conda
===========================================

Can be installed as a package with conda, using::

    conda install -c conda-forge BubbleDet

See the `BubbleDet Conda page`_ for further information, or the
`BubbleDet feedstock`_ for conda-forge maintenance.

.. _BubbleDet Conda page: https://anaconda.org/conda-forge/bubbledet

.. _BubbleDet feedstock: https://github.com/conda-forge/bubbledet-feedstock
