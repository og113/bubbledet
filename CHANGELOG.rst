Changelog
=========

1.0.0 (2023-08-30)
------------------

* First public release of BubbleDet, together with submission to arXiv.


0.5.0 (2023-08-29)
------------------

* Automated extrapolation, and improved error estimates.


0.4.0 (2023-08-28)
------------------

* Added findDeterminant functionality for d=1.


0.3.0 (2023-08-17)
------------------

* Removed the CosmoTransitions fork.


0.2.0 (2023-08-17)
------------------

* Upgraded algorithm for dealing with massless bounces.


0.1.0 (2023-07-31)
------------------

* Restructured to handle multi-particle determinants.


0.0.1 (2022-01-23)
------------------

* Project initiated.
